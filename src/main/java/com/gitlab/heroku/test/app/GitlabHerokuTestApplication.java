package com.gitlab.heroku.test.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabHerokuTestApplication {

  public static void main(String[] args) {
    SpringApplication.run(GitlabHerokuTestApplication.class, args);
  }
}
