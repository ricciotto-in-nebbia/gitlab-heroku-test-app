package com.gitlab.heroku.test.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerTest {

  @GetMapping("/")
  public String sayHello(){
    return "-- Hello World. Отправлено через Gitlab-2. -- ";
  }

}
